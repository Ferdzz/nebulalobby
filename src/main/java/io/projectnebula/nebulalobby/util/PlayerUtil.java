package io.projectnebula.nebulalobby.util;

import java.util.UUID;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class PlayerUtil {

	public static String getPlayerPrefix(UUID player) {
        String prefix = "" + ChatColor.GRAY;
        String staffChar = "\u2756";
        String donateChar = "\u274B";
        Permission perms = Bukkit.getServer().getServicesManager().getRegistration(Permission.class).getProvider();
        String rank = perms.getPrimaryGroup(Bukkit.getPlayer(player));
       
        if(rank.equals("moderator") || rank.equals("admin"))
                prefix += ChatColor.GOLD + staffChar + ChatColor.BLUE;
        else if (rank.equals("elite"))
                prefix += ChatColor.DARK_PURPLE + donateChar + ChatColor.BLUE;
        else if (rank.equals("mvp"))
                prefix += ChatColor.YELLOW + donateChar + ChatColor.BLUE;
        else if (rank.equals("vip"))
                prefix += ChatColor.GREEN + donateChar + ChatColor.BLUE;
        return prefix;
    }
	
}
