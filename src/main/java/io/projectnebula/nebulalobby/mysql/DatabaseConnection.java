package io.projectnebula.nebulalobby.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;

public class DatabaseConnection {

	private Connection con;
	private PreparedStatement pst;
	private Statement st;
	
	private String username;
	private String password;
	private String url;
	
	public DatabaseConnection(String username, String password, String url){
		this.username = username;
		this.password = password;
		this.url = url;
	}
	
	public void connect(){
		try{
            con = DriverManager.getConnection(url, username, password);            
        }catch(SQLException ex){
            Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);          
        }
	}
	
	public void addNewUser(String username){
        try{
        	if(con.isClosed()){
        		connect();
        	}
        	int atoms = 0;
			pst = con.prepareStatement("INSERT INTO PlayerStats(username, kills, deaths, atoms) VALUES(?, ?, ?, ?)");
	        pst.setString(1, username);
	        pst.setInt(2, 0);
	        pst.setInt(3, 0);
	        pst.setInt(4, atoms);
	        pst.executeUpdate();
		}catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}finally{
            try {
                if (pst != null) {
                	pst.close();
                }
            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
                lgr.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
	}	
	
	public void updateValue(String username, String item, int val){
		try {
			if(con.isClosed()){
        		connect();
        	}
            pst = con.prepareStatement("UPDATE PlayerStats SET " + item + " = ? "
                    + "WHERE username = ?");
            pst.setInt(1, val);
            pst.setString(2, username);         
            pst.executeUpdate();
            
        }catch(SQLException ex) {
            Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        }finally{
            try{
                if(st != null){
                    st.close();
                }

            }catch(SQLException ex){
                Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
	}
	
	public int getValue(String username, String item){
		int val = -1;
		try {
			if(con.isClosed()){
        		connect();
        	}
            pst = con.prepareStatement("SELECT * FROM PlayerStats WHERE username = ?");
            pst.setString(1, username);         
  
            ResultSet rs = pst.executeQuery();
            
            while(rs.next()){
            	val = rs.getInt(item);
            }
            
        }catch(SQLException ex) {
            Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        }finally{
            try{
                if(st != null){
                    st.close();
                }

            }catch(SQLException ex){
                Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return val;
	}
	
	public String getValueString(String username, String item){
		String val = " ";
		try {
			if(con.isClosed()){
        		connect();
        	}
            pst = con.prepareStatement("SELECT * FROM PlayerStats WHERE username = ?");
            pst.setString(1, username);         
  
            ResultSet rs = pst.executeQuery();
            
            while(rs.next()){
            	val = rs.getString(item);
            }
            
        }catch(SQLException ex) {
            Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        }finally{
            try{
                if(st != null){
                    st.close();
                }

            }catch(SQLException ex){
                Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return val;
	}
	
	public boolean doesDataBaseContain(String username){
		boolean val = false;
		try {
			if(con.isClosed()){
        		connect();
        	}
            pst = con.prepareStatement("SELECT * FROM PlayerStats WHERE username = ?");
            pst.setString(1, username);         
  
            ResultSet rs = pst.executeQuery();
            
            if(rs.next()){
            	val = true;
            }
            
        }catch(SQLException ex) {
            Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
            return false;
        }finally{
            try{
                if(st != null){
                    st.close();
                }

            }catch(SQLException ex){
                Logger lgr = Logger.getLogger(DatabaseConnection.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return val;
	}
	
}
