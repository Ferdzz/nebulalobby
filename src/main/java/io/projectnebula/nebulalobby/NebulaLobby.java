package io.projectnebula.nebulalobby;

import io.projectnebula.nebulalobby.atoms.Atoms;
import io.projectnebula.nebulalobby.commands.EssentialCommands;
import io.projectnebula.nebulalobby.mysql.DatabaseConnection;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.ChatColor;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissionsException;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.CommandsManager;
import com.sk89q.minecraft.util.commands.MissingNestedCommandException;
import com.sk89q.minecraft.util.commands.WrappedCommandException;

public class NebulaLobby extends JavaPlugin {

	private CommandsManager<CommandSender> commands;
	public static DatabaseConnection dbConnection = new DatabaseConnection("root", "1ESfQsiW", "jdbc:mysql://projectnebula.io/mcstats");

	@Override
	public void onEnable() {
		dbConnection.connect();
		setupCommands();
		Bukkit.getPluginManager().registerEvents(new Atoms(), this);
		Bukkit.getPluginManager().registerEvents(new EssentialCommands(), this);
	}

	private void setupCommands() {
		this.commands = new CommandsManager<CommandSender>() {
			@Override
			public boolean hasPermission(CommandSender sender, String perm) {
				return sender instanceof ConsoleCommandSender || sender.hasPermission(perm);
			}
		};
		CommandsManagerRegistration cmdRegister = new CommandsManagerRegistration(this, this.commands);
		cmdRegister.register(EssentialCommands.class);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		try {
			this.commands.execute(cmd.getName(), args, sender, sender);
		} catch (CommandPermissionsException e) {
			sender.sendMessage(ChatColor.RED + "You do not have the permission to use this command.");
		} catch (MissingNestedCommandException e) {
			sender.sendMessage(ChatColor.RED + e.getUsage().replace("{cmd}", cmd.getName()));
		} catch (CommandUsageException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
			sender.sendMessage(ChatColor.RED + e.getUsage());
		} catch (WrappedCommandException e) {
			if (e.getCause() instanceof NumberFormatException) {
				sender.sendMessage(ChatColor.RED + "Number expected, string received instead");
			} else {
				sender.sendMessage(ChatColor.RED + "Unknown error, please contact us for further information");
				e.printStackTrace();
			}
		} catch (CommandException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
		}
		return true;
	}
}
