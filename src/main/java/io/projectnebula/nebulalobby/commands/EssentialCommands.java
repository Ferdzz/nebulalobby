package io.projectnebula.nebulalobby.commands;

import io.projectnebula.nebulalobby.NebulaLobby;
import io.projectnebula.nebulalobby.atoms.Atoms.ChangeReason;
import io.projectnebula.nebulalobby.events.AtomChangeEvent;
import io.projectnebula.nebulalobby.util.PlayerUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;

public class EssentialCommands implements Listener {

	/*
	 * HDM
	 */
	static List<UUID> messagesOff = new ArrayList<UUID>();
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		String message = e.getDeathMessage();
		e.setDeathMessage(null);

		for (UUID s : messagesOff) {
			Player p = Bukkit.getPlayer(s);
			p.sendMessage(message);
		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e3) {
		if (messagesOff.contains(e3.getPlayer().getName())) {
			messagesOff.remove(e3.getPlayer().getName());
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent e){
		e.setFormat("%s: %s");
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		if(!NebulaLobby.dbConnection.doesDataBaseContain(e.getPlayer().getName())){
			e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE + "Welcome " + e.getPlayer().getName() + "!");
			NebulaLobby.dbConnection.addNewUser(e.getPlayer().getName());		
		}
		e.getPlayer().setDisplayName(PlayerUtil.getPlayerPrefix(e.getPlayer().getUniqueId()) + e.getPlayer().getName() + ChatColor.WHITE);
	}
	
	@Command(aliases = { "hdm" }, desc = "Toggle death messages")
	public static boolean hdm(CommandContext cmd, CommandSender sender) {
		if(!(sender instanceof Player))
			return false;
		
		Player p = (Player) sender;
		p.sendMessage(ChatColor.GRAY + "Death messages have been deactivated! Use /hdm to reactivate them!");

		UUID uuid = p.getUniqueId();
		if (messagesOff.contains(uuid)) {
			p.sendMessage(ChatColor.GRAY + "Death messages have been activated! Use /hdm to deactivate them!");
			messagesOff.remove(uuid);
		} else {
			messagesOff.add(uuid);
		}

		return true;
	}

	/*
	 * Rules
	 */
	@Command(aliases = { "rules" }, desc = "Gives rules to a player")
	public static boolean rules(CommandContext cmd, CommandSender sender) {
		sender.sendMessage("§2Be respectful §eand be a good sport.");
		sender.sendMessage("§cDon't spawn camp§e.");
		sender.sendMessage("§eDo not §cspam chat§e, or §cadvertise §enon-Nebula servers§e.");
		sender.sendMessage("§eDo not §cabuse glitches §eor use client mods to §ccheat§e.");
		sender.sendMessage("§eDo not §cteam grief§e: team kill, destroy own base, etc§e.");
		sender.sendMessage("§eDo not be §creckless §ewith TNT or limited resources§e.");
		sender.sendMessage("§eKnow the §cmap rules§e.");
		sender.sendMessage("§eAll infractions are subject to §cstaff discretion§e.");
		return true;
	}

	/*
	 * Help
	 */
	@Command(aliases = { "help" }, desc = "Lists all commands to be used by player")
	public static boolean help(CommandContext cmd, CommandSender sender) {
		
		int page = 1;
		if (cmd.argsLength() != 0)
			try {
				page = Integer.parseInt(cmd.getString(0));
			} catch (Exception e) {
				sender.sendMessage(ChatColor.RED + "Please enter a correct page number");
				return true;
			}

		switch (page) {
		case 1:
			sender.sendMessage("§bPage 1 out of 2.");
			sender.sendMessage("§6/rules: §aGives a list of rules to the server.");
			sender.sendMessage("§6/all: §aAllows you to message all the players on the server.");
			sender.sendMessage("§6/match: §aShows to game time, current winning team, map information, and server.");
			sender.sendMessage("§6/staff: §aShows online staff.");
			sender.sendMessage("§6/msg: §aMessages a player.");
			sender.sendMessage("§6/website: §aGives website link.");
			sender.sendMessage("§buse /help 2 For More Commands.");
			break;
		case 2:
			sender.sendMessage("§bPage 2 out of 2.");
			sender.sendMessage("§6/r: §aReply to a recent message.");
			sender.sendMessage("§6/find: §aFind a specific player and check if they are online or offline.");
			sender.sendMessage("§6/hdm: §aHide death messages in the chat.");
			sender.sendMessage("§6/lookup: §aShows if a player has been banned or kicked, and who preformed the action.");
			sender.sendMessage("§6/report: §aAllows you to report a certain player for breaking the rules.");
			break;
		default:
			sender.sendMessage(ChatColor.RED + "Please enter a correct page number");
		}
		return true;
	}

	/*
	 * Find
	 */
	@Command(aliases = { "find" }, desc = "Checks if a player is online or not", usage = "[player]", min = 1)
	public static boolean find(CommandContext cmd, CommandSender sender, String[] args) {
		Player targetplayer = Bukkit.getPlayer(args[0]);

		if (targetplayer.isOnline()) {
			sender.sendMessage(ChatColor.GREEN + targetplayer.getName() + "  is online!");
		} else {
			sender.sendMessage(ChatColor.RED + targetplayer.getName() + "  is not online!");
		}
		return true;
	}

	/*
	 * Staff
	 */
	@Command(aliases = { "staff" }, desc = "Lists all online staff")
	public static boolean staff(CommandContext cmd, CommandSender sender) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (player.hasPermission("nebula.staff")) {
				sender.sendMessage(ChatColor.GOLD + player.getName() + ChatColor.GRAY + ", part of the " + Bukkit.getServer().getServicesManager().getRegistration(Permission.class).getProvider().getPrimaryGroup(player) + "s. ");
			}
		}
		return true;
	}
	
	/*
	 * Atoms
	 */
	@Command(aliases = {"atom", "atoms" }, desc = "View your own or another player's snowflake count.", usage = "[player]")
	public static boolean atoms(CommandContext cmd, CommandSender sender) {
		if (cmd.argsLength() == 0) {
			sender.sendMessage(ChatColor.DARK_PURPLE + "You have " + ChatColor.GOLD + NebulaLobby.dbConnection.getValue(sender.getName(), "atoms") + " atoms");
		} else if (cmd.argsLength() == 1 && (cmd.getString(0).equalsIgnoreCase("send") || (cmd.getString(0).equalsIgnoreCase("give") && sender.isOp()))) { // Describes the command usage
			if(cmd.getString(0).equalsIgnoreCase("send")) {
				sender.sendMessage(ChatColor.DARK_PURPLE + "Transfers atoms from one player to another");
				sender.sendMessage(ChatColor.DARK_PURPLE + "Usage: " + ChatColor.GOLD + "/atoms send [amount] [player]");
			} else {
				sender.sendMessage(ChatColor.DARK_PURPLE + "Spawns new atoms to give to a player");
				sender.sendMessage(ChatColor.DARK_PURPLE + "Usage: " + ChatColor.GOLD + "/atoms give [amount] [player]");
			}
		} else if (cmd.argsLength() == 1) {
			String name = Bukkit.getOfflinePlayer(cmd.getString(0)).getName();
			if(NebulaLobby.dbConnection.doesDataBaseContain(name)){
				sender.sendMessage(ChatColor.DARK_PURPLE + name + "'s balance: " + ChatColor.GOLD + NebulaLobby.dbConnection.getValue(name, "atoms") + " atoms");
			}
		} else if (sender instanceof Player && cmd.argsLength() == 3 && cmd.getString(0).equalsIgnoreCase("send") && NebulaLobby.dbConnection.getValue(sender.getName(), "atoms") >= Integer.parseInt(cmd.getString(1)) && Integer.parseInt(cmd.getString(1)) > 0) {
			int sent = Integer.parseInt(cmd.getString(1));
			Bukkit.getServer().getPluginManager().callEvent(new AtomChangeEvent((Player) sender, ChangeReason.PAID_GIVEN, -sent, cmd.getString(2)));
			Bukkit.getServer().getPluginManager().callEvent(new AtomChangeEvent(Bukkit.getOfflinePlayer(cmd.getString(2)).getPlayer(), ChangeReason.PAID_RECEIVED, sent, sender.getName()));
		} else if (sender instanceof Player && cmd.argsLength() == 3 && cmd.getString(0).equalsIgnoreCase("send") && !(NebulaLobby.dbConnection.getValue(sender.getName(), "atoms") >= Integer.parseInt(cmd.getString(1))) || !(Integer.parseInt(cmd.getString(1)) > 0)) {
			sender.sendMessage(ChatColor.RED + "Insufficient atoms");
		} else if (sender.isOp() && cmd.argsLength() == 3 && cmd.getString(0).equalsIgnoreCase("give")) {
			int sent = Integer.parseInt(cmd.getString(1));
			Bukkit.getServer().getPluginManager().callEvent(new AtomChangeEvent(Bukkit.getOfflinePlayer(cmd.getString(2)).getPlayer(), ChangeReason.PAID_RECEIVED, sent, sender.getName()));
		}

		return true;
	}
}
