package io.projectnebula.nebulalobby.atoms;

import io.projectnebula.nebulalobby.NebulaLobby;
import io.projectnebula.nebulalobby.events.AtomChangeEvent;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class Atoms implements Listener {

    @EventHandler
    public void onAtomChange(AtomChangeEvent event) {
        if (event.getFinalAmount() != 0) {
            String reason = "";
            if(event.getChangeReason().equals(ChangeReason.PAID_GIVEN)){
            	reason = "you paid " + event.get(0);
            }else if(event.getChangeReason().equals(ChangeReason.PAID_RECEIVED)){
            	reason = event.get(0) + " paid you";
            }
            if(event.getFinalAmount() > 0)
            	event.getPlayer().sendMessage((ChatColor.GREEN + "" + ChatColor.BOLD + "+" + event.getFinalAmount() + ChatColor.AQUA + " Atoms" + ChatColor.DARK_PURPLE + " | " + ChatColor.GOLD + "" + ChatColor.ITALIC + event.getMultiplier() + "x" + ChatColor.DARK_PURPLE + " | " + ChatColor.GRAY + reason));
            else
            	event.getPlayer().sendMessage((ChatColor.GREEN + "" + ChatColor.BOLD + event.getFinalAmount() + ChatColor.AQUA + " Atoms" + ChatColor.DARK_PURPLE + " | " + ChatColor.GOLD + "" + ChatColor.ITALIC + event.getMultiplier() + "x" + ChatColor.DARK_PURPLE + " | " + ChatColor.GRAY + reason));
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.LEVEL_UP, 1, 1.5F);
            NebulaLobby.dbConnection.updateValue(event.getPlayer().getName(), "atoms", NebulaLobby.dbConnection.getValue(event.getPlayer().getName(), "atoms") + event.getFinalAmount());
        }
    }

    public enum ChangeReason {
        PLAYER_KILL(), WOOL_TOUCH(), WOOL_PLACE(), CORE_LEAK(), MONUMENT_DESTROY(), TEAM_WIN(), TEAM_LOYAL(), DESTROY_WOOL(), PAID_GIVEN(), PAID_RECEIVED(), FLAG_PICKUP(), FLAG_CAPTURE(), HILL_CAPTURE()
    }
	
}
