package io.projectnebula.nebulalobby.events;

import io.projectnebula.nebulalobby.atoms.Atoms;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class AtomChangeEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final Player player;
    private final Atoms.ChangeReason changeReason;
    private final int rawAmount;
    private final double multiplier;
    private final int finalAmount;
    private final String[] args;

    public AtomChangeEvent(Player player, Atoms.ChangeReason changeReason, int rawAmount, String... args) {
        this.player = player;
        this.changeReason = changeReason;
        this.rawAmount = rawAmount;
        this.multiplier = 1;
        this.finalAmount = (int) (rawAmount * multiplier);
        this.args = args;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public int getRawAmount() {
        return rawAmount;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public int getFinalAmount() {
        return finalAmount;
    }

    public String get(int i) {
        return args[i];
    }

    public Atoms.ChangeReason getChangeReason() {
        return changeReason;
    }
}
